# Copyright (c) 2018-2019 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gio, Gtk, GLib

from passbook.define import App


class ApplicationActions:
    """
        Application actions
    """

    def __init__(self):
        """
            Init actions
        """
        add_collection_action = Gio.SimpleAction.new("add_collection", None)
        add_collection_action.connect("activate",
                                      self.__on_add_collection_activate)
        App().add_action(add_collection_action)

        enable_colors = App().settings.get_value("enable-colors")
        colors_action = Gio.SimpleAction.new_stateful("enable_colors",
                                                      None,
                                                      enable_colors)
        colors_action.connect("change-state", self.__on_colors_change_state)
        App().add_action(colors_action)

        about_action = Gio.SimpleAction.new("about", None)
        about_action.connect("activate", self.__on_about_activate)
        App().add_action(about_action)

        shortcuts_action = Gio.SimpleAction.new("shortcuts", None)
        shortcuts_action.connect("activate", self.__on_shortcuts_activate)
        App().add_action(shortcuts_action)

        shortcut_action = Gio.SimpleAction.new("shortcut",
                                               GLib.VariantType.new("s"))
        shortcut_action.connect("activate", self.__on_shortcut_activate)
        App().add_action(shortcut_action)

        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", lambda x, y: self.quit())
        App().add_action(quit_action)

        App().set_accels_for_action("app.shortcut::new_password",
                                    ["<Control>n"])
        App().set_accels_for_action("app.add_collection",
                                    ["<Control><Shift>n"])
        App().set_accels_for_action("app.enable_colors",
                                    ["<Control><Shift>c"])

#######################
# PRIVATE             #
#######################
    def __on_add_collection_activate(self, action, param):
        """
            Show shorctus
            @param action as Gio.SimpleAction
            @param param as GLib.Variant
        """
        from passbook.dialog_add_collection import AddCollectionDialog
        dialog = AddCollectionDialog()
        dialog.run()

    def __on_about_activate(self, action, param):
        """
            Setup about dialog
            @param action as Gio.SimpleAction
            @param param as GLib.Variant
        """
        builder = Gtk.Builder()
        builder.add_from_resource("/org/gnome/Passbook/AboutDialog.ui")
        about = builder.get_object("about_dialog")
        about.set_transient_for(App().window)
        about.connect("response", lambda x, y: about.destroy())
        about.show()

    def __on_shortcuts_activate(self, action, param):
        """
            Show shorctus
            @param action as Gio.SimpleAction
            @param param as GLib.Variant
        """
        builder = Gtk.Builder()
        builder.add_from_resource("/org/gnome/Passbook/Shortcuts.ui")
        builder.get_object("shortcuts").set_transient_for(App().window)
        builder.get_object("shortcuts").show()

    def __on_colors_change_state(self, action, value):
        """
            Enable/Disable colors
            @param action as Gio.SimpleAction
            @param value as GLib.Variant
        """
        App().settings.set_value("enable-colors", value)
        action.set_state(value)
        if not value:
            App().css_provider.load_from_data(b"")
        App().window.update_color()

    def __on_shortcut_activate(self, action, param):
        """
            Handle shortcuts
            @param action as Gio.SimpleAction
            @param param as GLib.Variant
        """
        string = param.get_string()
        if string == "new_password":
            self.window.on_add_password_clicked()
