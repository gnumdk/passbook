# Copyright (c) 2018-2019 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk

from passbook.window_content import WindowContent
from passbook.window_events import WindowEvents
from passbook.window_adaptive import AdaptiveWindow


@Gtk.Template.from_resource("/org/gnome/Passbook/MainWindow.ui")
class Window(Gtk.ApplicationWindow, WindowContent,
             WindowEvents, AdaptiveWindow):
    """
        Main window
    """
    __gtype_name__ = "MainWindow"
    header_bar = Gtk.Template.Child()
    add_password_button = Gtk.Template.Child()
    collections_listbox = Gtk.Template.Child()
    keepass_listbox = Gtk.Template.Child()
    applications_listbox = Gtk.Template.Child()
    collections_separator = Gtk.Template.Child()
    keepass_separator = Gtk.Template.Child()
    applications_separator = Gtk.Template.Child()
    secrets_listbox = Gtk.Template.Child()
    back_button = Gtk.Template.Child()
    color_button = Gtk.Template.Child()
    collection_header_bar = Gtk.Template.Child()
    collection_count_label = Gtk.Template.Child()
    collection_infobar = Gtk.Template.Child()
    infobar_button = Gtk.Template.Child()
    collection_delete_button = Gtk.Template.Child()
    stack = Gtk.Template.Child()
    paned = Gtk.Template.Child()
    paned_child = Gtk.Template.Child()
    paned_stack = Gtk.Template.Child()
    paned_spinner = Gtk.Template.Child()
    filter_bar = Gtk.Template.Child()
    filter_entry = Gtk.Template.Child()
    search_button = Gtk.Template.Child()
    placeholder_label = Gtk.Template.Child()
    placeholder_image = Gtk.Template.Child()
    groups_button = Gtk.Template.Child()

    def __init__(self, app):
        """
            Get a new window
            @param app as Gtk.Application
        """
        Gtk.ApplicationWindow.__init__(self, application=app)
        AdaptiveWindow.__init__(self)
        WindowContent.__init__(self)
        WindowEvents.__init__(self)
        self.add_stack(self.stack)
        self.add_paned(self.paned, self.paned_stack)
        # Set window actions
        # shortcut_action = Gio.SimpleAction.new("shortcut",
        #                                       GLib.VariantType.new("s"))
        # shortcut_action.connect("activate", self.__on_shortcut_action)
        # self.add_action(shortcut_action)
        self.set_auto_startup_notification(False)

    def go_back(self):
        """
            Hide headerbar
        """
        self.collection_header_bar.hide()
        AdaptiveWindow.go_back(self)

#############
# CALLBACKS #
#############
    @Gtk.Template.Callback()
    def on_row_activated(self, listbox, row):
        WindowContent.on_row_activated(self, listbox, row)

    @Gtk.Template.Callback()
    def on_add_password_clicked(self, *ignore):
        WindowEvents.on_add_password_clicked(self)

    @Gtk.Template.Callback()
    def on_menu_clicked(self, button):
        WindowEvents.on_menu_clicked(self, button)

    @Gtk.Template.Callback()
    def on_collection_delete_confirm_button_clicked(self, button):
        WindowEvents.on_collection_delete_confirm_button_clicked(
            self, button)

    @Gtk.Template.Callback()
    def on_collection_delete_button_clicked(self, button):
        WindowEvents.on_collection_delete_button_clicked(self, button)

    @Gtk.Template.Callback()
    def on_infobar_response(self, infobar, response_id):
        WindowEvents.on_infobar_response(self, infobar, response_id)

    @Gtk.Template.Callback()
    def on_color_set(self, button):
        WindowContent.on_color_set(self, button)

    @Gtk.Template.Callback()
    def on_configure_event(self, window, event):
        WindowEvents.on_configure_event(self, window, event)

    @Gtk.Template.Callback()
    def on_window_state_event(self, window, event):
        WindowEvents.on_window_state_event(self, window, event)

    @Gtk.Template.Callback()
    def on_back_button_clicked(self, button):
        WindowEvents.on_back_button_clicked(self, button)

    @Gtk.Template.Callback()
    def on_search_button_toggled(self, button):
        WindowEvents.on_search_button_toggled(self, button)

    @Gtk.Template.Callback()
    def on_filter_entry_changed(self, entry):
        WindowEvents.on_filter_entry_changed(self, entry)

    @Gtk.Template.Callback()
    def on_key_press_event(self, window, event):
        WindowEvents.on_key_press_event(self, window, event)

    @Gtk.Template.Callback()
    def on_groups_button_clicked(self, button):
        WindowEvents.on_groups_button_clicked(self, button)

#############
# Protected #
#############
    def _set_adaptive_stack(self, b):
        """
            Move paned child to stack
            @param b as bool
        """
        if b and not self._adaptive_stack:
            pass
        elif not b and self._adaptive_stack:
            if self.applications_listbox.get_selected_row() is not None:
                self.stack.set_visible_child_name("secrets")
        AdaptiveWindow._set_adaptive_stack(self, b)

############
# Private  #
############
