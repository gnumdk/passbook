# Copyright (c) 2018-2019 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Secret

from passbook.logger import Logger


class KeepassSecret:
    """
        For API compatiblity with Secret.Item
    """

    def __init__(self, collection, kp_item):
        """
            Init secret
            @param collection as KeepassCollection
            @param kp_item as pykeepass.entry.Entry
        """
        self.__collection = collection
        self.__item = kp_item

    def get_secret(self):
        """
            Get secret
            @return Secret.Value
        """
        value = Secret.Value(self.__item.password,
                             len(self.__item.password),
                             "text/plain")
        return value

    def get_name(self):
        """
            Get item name
            @return str
        """
        return self.__item.title

    def get_label(self):
        """
            Get label
            @return str
        """
        return self.__item.title

    def get_attributes(self):
        """
            Get attributes
            @return {}
        """
        return {"login": self.__item.username}

    def set_secret(self, value, cancellable, callback, *args):
        """
            Set item secret
            @param value as Secret.Value
        """
        try:
            self.__item.password = value.get_text()
            callback(self, True, *args)
        except Exception as e:
            Logger.error("KeepassSecret::set_secret(): %s" % e)

    def set_secret_finish(self, *ignore):
        """
            Save db
            @param item as pykeepass.entry.Entry
        """
        self.__collection.save()

    def set_attributes(self, schema, attributes, cancellable, callback, *args):
        """
            Set item attributes
            @param schema as {}
            @param attributes as {}
            @param cancellable as Gio.Cancellable
            @param callback as function
        """
        try:
            if "login" in attributes.keys():
                self.__item.username = attributes["login"]
                # Not needed as we always save login and password later
                # callback(self, True, *args)
        except Exception as e:
            Logger.error("KeepassSecret::set_attributes(): %s" % e)

    def set_attributes_finish(self, *ignore):
        """
            Save db
            @param item as pykeepass.entry.Entry
        """
        self.__collection.save()

    def delete(self, cancellable, callback, save=True, *args):
        """
            Delete item (save needed)
            @param cancellable as Gio.Cancellable
            @param callback as Gio.Callback
            @param save as bool
        """
        try:
            self.__item.delete()
            if save:
                self.__collection.save()
        except Exception as e:
            Logger.error("KeepassSecret::delete(): %s" % e)

    @property
    def collection(self):
        """
            Get item collection
            @return KeepassCollection
        """
        return self.__collection

    @property
    def kp_item(self):
        """
            Get kp item
            @return pykeepass.entry.Entry
        """
        return self.__item
