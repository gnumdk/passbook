# Copyright (c) 2018-2019 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gio

from gettext import gettext as _


App = Gio.Application.get_default


class Type:
    APPS = 0
    KEYRING = 1
    KEEPASS = 2


class LoadingType:
    SECRETS = 0
    SEARCH = 1


COLORS = ["rgb(255,140,130)", "rgb(255,194,125)", "rgb(255,243,148)",
          "rgb(209,255,130)", "rgb(140,213,255)", "rgb(228,198,250)",
          "rgb(163,144,124)", "rgb(149,163,171)"]


LABELS = {
    "Skype for Desktop":
        ("Skype", ["com.skype.Client", "skype"]),
    "fractal": ("Fractal", ["org.gnome.Fractal", "fractal"]),
}

SCHEMAS = {
    "chrome_libsecret_password_schema":
        ("Chromium", ["chromium", "chromium-browser", "google-chrome"]),
    "chrome_libsecret_os_crypt_password_v2":
        ("Chromium", ["chromium", "chromium-browser", "google-chrome"]),
    "_chrome_dummy_schema_for_unlocking":
        ("Chromium", ["chromium", "chromium-browser", "google-chrome"]),
    "org.gnome.keyring.NetworkPassword": ("Network", ["network-idle"]),
    "org.freedesktop.NetworkManager.Connection.Openconnect":
        (_("Network"), ["network-idle"]),
    "org.remmina.Password":
        ("Remmina", ["org.remmina.Remmina", "remmina"]),
    "org.gnome.Evolution.Data.Source":
        ("Evolution", ["evolution", "emblem-mail"]),
    "org.gnome.Eolie": ("Eolie", ["org.gnome.Eolie", "web-browser"]),
    "org.gnome.Lollypop": ("Lollypop", ["org.gnome.Lollypop"]),
    "org.gnome.Geary": ("Geary", ["org.gnome.Geary", "emblem-mail"]),
    "org.gnome.OnlineAccounts": (_("Online accounts"), ["emblem-web"]),
    "org.gnome.Boxes": ("Boxes", ["org.gnome.Boxes"]),
    "org.epiphany.FormPassword": ("Web", ["org.gnome.Epiphany"]),
}
