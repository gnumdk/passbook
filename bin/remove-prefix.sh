#!/bin/sh

# This is a workaround. Meson's python.get_path no longer removes the prefix (/usr/ usually)
# since the move from module python3 to python. This means that with python.get_path('purelib'),
# for example, it returns /usr/lib/python3.7 but in flatpak this won't work since /usr is read-only.
# This is where we would want to have /app/lib/python3.7. Hence this script since meson doesn't provide
# anything convenient to remove /usr from the path.

echo $1 | sed 's/\/usr\///'
