#!/bin/sh

### This script generates the python dependencies flatpak manifest inside the development runtime
### Run this script from the root of the project to update the pythons deps from the requirements.txt
### $ bin/update-pypi-deps-manifest.sh

GENERATOR_COMMANDS="""./bin/flatpak-pip-generator.py --requirements-file requirements.txt --output pypi-deps"""

### Tweak this to be the same as the runtime version in the flatpak manifest
### It might be possible to grab this with a script from the manifest but would be complicated for not much
RUNTIME_VERSION=3.36

### We run in the SDK to have headers and compilers (i.e. gcc) and ensure that
### we have enough permission from the host, the project directory and network basically
### And don't forget to start at the project root directory (cwd)
echo $GENERATOR_COMMANDS | flatpak run --cwd=$PWD --share=network --filesystem=$PWD org.gnome.Sdk//$RUNTIME_VERSION
