# Passbook password manager

**Passbook is looking for a new maintainer. (and need to be ported to GTK4)**

If an application is not supported by Passbook:
- Run` passbook -d` and open an issue with "Unknown schema:" lines.
- If the line is org.freedesktop.Secret.Generic, give me the password title from Seahorse.

# When running as a Flatpak, Passbook will be unable to access other Flatpak icons.

## Depends on

- `gtk3 >= 3.20`
- `gobject-introspection`
- `appstream-glib`
- `python3`
- `meson >= 0.40`
- `ninja`
- `python-cairo`
- `python-gobject`
-  pykeepass (Optional)

## Building from Git

```bash
$ git clone https://gitlab.gnome.org/gnumdk/passbook.git
$ cd passbook
$ meson builddir --prefix=/usr
$ sudo ninja -C builddir install
```

### On Debian GNU/Linux (Jessie)

```bash
# apt-get install meson libglib2.0-dev yelp-tools libgirepository1.0-dev libgtk-3-dev
```

### On Fedora

```bash
# sudo dnf install meson glib2-devel yelp-tools gtk3-devel gobject-introspection-devel python3
```
